<?php
namespace svk\Ydb;
class YDB
{
    private $transaction_id;
    private $endpoint;
    private $token;
    private $database;
    private $affected_rows;


    public function __construct($endpoint, $token, $database)
    {
        $this->endpoint = $endpoint;
        $this->token = $token;
        $this->database = $database;

        $this->DiscoveryService = new \svk\YdbSdk\DiscoveryService($endpoint, $token, $database);

        $this->TableService = new \svk\YdbSdk\TableService($endpoint, $token, $database);

        $this->SchemeService = new \svk\YdbSdk\SchemeService($endpoint, $token, $database);



        return true;
    }

    public function whoami()
    {
        return $this->DiscoveryService->WhoAmI();
    }

    public function ls($path, $recursive=false)
    {
        $res = $this->SchemeService->listDirectory($path)->getChildren();

        $result = [];

        if(substr($path,0,1) == '/')
            $path = substr($path,1);

        foreach($res as $k=>$v)
        {
            if(is_object($v))
            {
                $row = json_decode($v->serializeToJsonString(), true);
                
                $result[] = [
                    'name' => $path ? $path.'/'.$row['name'] : $row['name'],
                    'owner' => isset($row['owner']) ? $row['owner'] : false,
                    'type' => $row['type'],
                ];
                if($recursive)
                {
                    if($row['type']=='DIRECTORY' && substr($row['name'],0,1)!=='.')
                    {
                        $result = array_merge($result, $this->ls($path.'/'.$row['name'], true));
                    }
                }
            }
        }

        return $result;
    }

    public function query($yql)
    {
        $this->TableService->createSession();
        if(!$this->transaction_id)
        {
            $this->transaction_id = $this->begin_transaction();
        }
        
        $result =  $this->TableService->query($yql, $this->transaction_id);

        if($result)
        {
            $resultObj = new YDBResult($result);

            return $resultObj;
        } else
        {
            return false;
        }
    }

    public function error()
    {
        return [
            'errorCode' => $this->TableService->getLastErrorCode(),
            'errorStr' => $this->TableService->getLastError(),
        ];
    }

    public function begin_transaction()
    {
        $this->transaction_id = $this->TableService->beginTransaction();
        return $this->transaction_id;
    }

    public function commit()
    {
        if($this->transaction_id)
            return $this->TableService->commitTransaction($this->transaction_id);
        else
            return false;
    }

    public function describeTable($table)
    {
        return $this->TableService->describeTable($table);
    }

    public function dropTable($table)
    {
        return $this->TableService->dropTable($table);
    }

    public function select_db($database)
    {
        $this->TableService = new \svk\YdbSdk\TableService($this->endpoint, $this->token, $database);

        $this->SchemeService = new  \svk\YdbSdk\SchemeService($this->endpoint, $this->token, $database);
    }

    public function query_info()
    {
        return '';
    }
}

class YDBResult
{
    private $data = [];
    public $numRows;

    private $iterator = 0;

    function __construct($data)
    {
        $resultSets = $data->getResultSets();

        $data = json_decode($resultSets->offsetGet(0)->serializeToJsonString(), true);

        $this->data = $data;
        $this->numRows = count($data['rows']);
    }

    public function fetch_assoc()
    {
        $result = [];

        if($this->iterator >= $this->numRows)
        {
            return false;
        }

        foreach($this->data['rows'][$this->iterator]['items'] as $k=>$v)
        {
            if(isset($v['bytesValue']))
            {
                $data = base64_decode($v['bytesValue']);
            }
            if(isset($v['uint64Value']))
            {
                $data = intval($v['uint64Value']);
            }
            $result[$this->data['columns'][$k]['name']] = $data;
        }
        
        $this->iterator++;

        return $result;
    }

    public function fetch_row()
    {
        $result = [];

        if($this->iterator >= $this->numRows)
        {
            return false;
        }

        foreach($this->data['rows'][$this->iterator]['items'] as $k=>$v)
        {
            if(isset($v['bytesValue']))
            {
                $data = base64_decode($v['bytesValue']);
            }
            if(isset($v['uint64Value']))
            {
                $data = intval($v['uint64Value']);
            }
            $result[] = $data;
        }
        
        $this->iterator++;

        return $result;
    }    
}
